regexp <- '^fixation in region.center.x = ([0-9.]+), region.center.y = ([0-9.]+)'

.globals <- new.env()

fixExractor <- function(filename, full_script = FALSE) {
    
    if(full_script != TRUE) {
        file <- load.one.eye(filename)
    }
    else {
        file <- load.one.eye(filename)
        .globals$eye_time <- file$samples
    }
    msgs_logic <- file$events$type == 'message'
    msgs_subset <- file$events[msgs_logic, c(3,10)]
    msgs_subset[str_detect(msgs_subset[[2]], regexp),]
}

medExractor <- function(filename, full_script = FALSE) {
    
    fixations <- fixExractor(filename, full_script)
    meds <- sapply(str_filter(fixations[,2], regexp),
                   function(x) as.numeric(x[c(2,3)]))
    fixations$medx <- meds[1,]
    fixations$medy <- meds[2,]
    invisible(fixations)
}

fixationEnder <- function(filename) {
    
    fix_frame <- medExractor(filename, full_script = TRUE)
    end_times <- c()
    multifix <- c()
    # WHOA <- FALSE
    
    for(i in 1:(length(fix_frame[,1])-1)) {
        ## сравнение последующих координат с медианой
        # диапазон времен
        
        # A <- Sys.time()
        
        time_range <- .globals$eye_time[.globals$eye_time$time > fix_frame$stTime[i],]
        
        for(n in (1:length(time_range[,1]))) {
            # print(n)
            if(is.na(time_range$x[n])) {
                end_times[i] <- time_range$time[n]
                multifix[i] <- FALSE
                break
                
            }
            else if(abs(fix_frame$medx[i]-time_range$x[n])>50) {
                end_times[i] <- time_range$time[n]
                multifix[i] <- FALSE
                break
            }
            else if(abs(fix_frame$medy[i]-time_range$y[n])>50) {
                end_times[i] <- time_range$time[n]
                multifix[i] <- FALSE
                break
            }
            else if(time_range$time[n] >= fix_frame$stTime[i+1]) {
                end_times[i] <- time_range$time[n]
                multifix[i] <- TRUE
                # WHOA <- TRUE
                break
            }
        }
        
        #                 B <- Sys.time()
        #                 message(sprintf("Fixation %d of %d. Time spent: %.3f sec; Results: %d, %s", i, length(fix_frame[,1]), unclass(B-A)[1], end_times[i]-fix_frame$stTime[i]+500, multifix[i]))
    }
    ## separate
    i <- length(fix_frame[,1])
    
    # A <- Sys.time()
    
    time_range <- .globals$eye_time[.globals$eye_time$time > fix_frame$stTime[i],]
    # print(sprintf(, i, length(fix_frame[,1])))
    for(n in (1:length(time_range[,1]))) {
        # print(n)
        if(is.na(time_range$x[n])) {
            end_times[i] <- time_range$time[n]
            multifix[i] <- FALSE
            break
        }
        else if(abs(fix_frame$medx[i]-time_range$x[n])>50) {
            end_times[i] <- time_range$time[n]
            multifix[i] <- FALSE
            break
        }
        else if(abs(fix_frame$medy[i]-time_range$y[n])>50) {
            end_times[i] <- time_range$time[n]
            multifix[i] <- FALSE
            break
        }
        else if(n == length(time_range$time)) {
            end_times[i] <- time_range$time[n]
            multifix[i] <- TRUE
            # WHOA <- TRUE
            break
        }
    }
    
    #         B <- Sys.time()
    #         message(sprintf("Fixation %d of %d. Time spent: %.3f sec; Results: %d, %s", i, length(fix_frame[,1]), unclass(B-A)[1], end_times[i]-fix_frame$stTime[i]+500, multifix[i]))
    
    fix_frame$endTime <- end_times
    fix_frame$multifix <- multifix
    #         if(WHOA == TRUE) {
    #                 message(sprintf('WHOOOOOA!!!!!, %s', filename))
    #         }
    invisible(fix_frame)
}

fixationEnderShort <- function(filename) {
    fix_frame <- fixationEnder(filename)
    fix_frame_short <- data.frame(Latency = sprintf("%.3f", A$endTime/1000),
                                  Type = rep("endFix", length(A$endTime)),
                                  stringsAsFactors = FALSE)
    fix_frame_short$Type[fix_frame$multifix == TRUE] <- 'multiFix'
    invisible(fix_frame_short)
}
